package com.yzs.linkgame.view;

public interface OnToolsChangeListener{
	 void onRefreshChanged(int count);
	 void onTipChanged(int count);
}
