package com.yzs.linkgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.yzs.linkgame.view.GameView;
import com.yzs.linkgame.view.OnStateListener;
import com.yzs.linkgame.view.OnTimerListener;
import com.yzs.linkgame.view.OnToolsChangeListener;

public class GameActivity extends Activity implements OnToolsChangeListener,OnTimerListener,
		OnStateListener {
	private ImageButton img_startPlay;
	private ImageView img_title;
	private ProgressBar progress;

	private MyDialog dialog;
	private ImageView clock;
	private GameView gameView = null;
	private ImageButton img_tip;
	private ImageButton img_refresh;
	private TextView text_refreshNum;
	private TextView text_tipNum;

	private Animation anim = null;

	private MediaPlayer player;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
				case 0:
					dialog = new MyDialog(GameActivity.this,gameView,"完成！",
							gameView.getTotalTime() - progress.getProgress() + 1);
					dialog.show();
					break;
				case 1:
					dialog = new MyDialog(GameActivity.this,gameView,"失败！",
							gameView.getTotalTime() - progress.getProgress() + 1);
					dialog.show();
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_view);
		anim =  AnimationUtils.loadAnimation(this, R.anim.shake);
		findView();
		GameView.initSound(this);
		startView();

		img_startPlay.setOnClickListener(new BtnClickListener());

		gameView.setOnTimerListener(this);
		gameView.setOnStateChangeListener(this);
		gameView.setOnToolsChangedListener(this);
		img_refresh.setOnClickListener(new BtnClickListener());
		img_tip.setOnClickListener(new BtnClickListener());
	}

	public void findView(){
		clock = (ImageView)this.findViewById(R.id.clock);
		progress = (ProgressBar)this.findViewById(R.id.timer);
		img_title = (ImageView)this.findViewById(R.id.title_img);
		img_startPlay = (ImageButton)this.findViewById(R.id.play_btn);
		img_tip = (ImageButton)this.findViewById(R.id.tip_btn);
		img_refresh = (ImageButton)this.findViewById(R.id.refresh_btn);
		gameView = (GameView)this.findViewById(R.id.game_view);
		text_refreshNum = (TextView)this.findViewById(R.id.text_refresh_num);
		text_tipNum = (TextView)this.findViewById(R.id.text_tip_num);
	}

	public void startView(){
		Animation scale = AnimationUtils.loadAnimation(this,R.anim.scale_anim);
		img_title.startAnimation(scale);
		img_startPlay.startAnimation(scale);

		player = MediaPlayer.create(this, R.raw.bg);
		player.setLooping(true);//设置循环播放
		player.start();
	}

	public void playingView(){
		Animation scaleOut = AnimationUtils.loadAnimation(this, R.anim.scale_anim_out);
		img_title.startAnimation(scaleOut);
		img_startPlay.startAnimation(scaleOut);
		img_title.setVisibility(View.GONE);
		img_startPlay.setVisibility(View.GONE);

		clock.setVisibility(View.VISIBLE);
		progress.setMax(gameView.getTotalTime());
		progress.setProgress(gameView.getTotalTime());
		progress.setVisibility(View.VISIBLE);
		gameView.setVisibility(View.VISIBLE);
		img_tip.setVisibility(View.VISIBLE);
		img_refresh.setVisibility(View.VISIBLE);
		text_tipNum.setVisibility(View.VISIBLE);
		text_refreshNum.setVisibility(View.VISIBLE);
		Animation animIn = AnimationUtils.loadAnimation(this, R.anim.trans_in);
		gameView.startAnimation(animIn);
		img_tip.startAnimation(animIn);
		img_refresh.startAnimation(animIn);
		text_tipNum.startAnimation(animIn);
		text_refreshNum.startAnimation(animIn);
		player.pause();
		gameView.startPlay();
		toast();
	}

	class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch(v.getId()){
				case R.id.play_btn:
					playingView();
					break;
				case R.id.refresh_btn:
					img_refresh.startAnimation(anim);
					gameView.refreshChange();
					gameView.invalidate();
					break;
				case R.id.tip_btn:
					img_tip.startAnimation(anim);
					gameView.autoHelp();
					break;
			}
		}
	}
	@Override
	public void onRefreshChanged(int count) {
		text_refreshNum.setText(""+gameView.getRefreshNum());
	}
	@Override
	public void onTipChanged(int count) {
		text_tipNum.setText("" + gameView.getTipNum());
	}
	@Override
	public void onTimer(int leftTime) {
		progress.setProgress(leftTime);

	}

	@Override
	public void OnStateChanged(int StateMode) {
		switch(StateMode){
			case GameView.WIN:
				handler.sendEmptyMessage(0);
				break;
			case GameView.LOSE:
				handler.sendEmptyMessage(1);
				break;
			case GameView.PAUSE:
				player.stop();
				gameView.player.pause();
				gameView.stopTimer();
				break;
			case GameView.QUIT:
				player.release();
				gameView.player.release();
				gameView.stopTimer();
				break;
			case GameView.CONTINUE:
				gameView.player.start();
				break;
		}
	}
	public void quit(){
		this.finish();
	}

	public void toast(){
		Toast.makeText(this, "游戏已经开始!总时间： " + gameView.getTotalTime() + "s", Toast.LENGTH_LONG).show();
	}
	@Override
	protected void onPause() {
		super.onPause();
		gameView.setMode(GameView.PAUSE);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		gameView.setMode(GameView.QUIT);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, 1, Menu.NONE,"Replay").setIcon(R.drawable.replay);
		menu.add(Menu.NONE, 2, Menu.NONE, "Pause").setIcon(R.drawable.pause);
		menu.add(Menu.NONE, 3, Menu.NONE,"Mute").setIcon(R.drawable.mute);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case 1:
				gameView.setTotalTime(100);
				progress.setMax(100);
				gameView.startPlay();
				break;
			case 2:
				gameView.stopTimer();
				AlertDialog.Builder dialog = new AlertDialog.Builder(this);
				dialog.setIcon(R.drawable.icon);
				dialog.setTitle("继续");
				dialog.setMessage("继续游戏?");
				dialog.setPositiveButton("继续", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						gameView.setContinue();
						player.start();
						gameView.player.start();
					}
				}).setNeutralButton("重玩", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						gameView.startPlay();
					}
				}).setNegativeButton("退出", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent startMain = new Intent(Intent.ACTION_MAIN);
						startMain.addCategory(Intent.CATEGORY_HOME);
						startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(startMain);
						System.exit(0);
					}
				});
				dialog.show();
				break;
			case 3:
				if(item.getTitle().equals("Mute")){
					item.setTitle("SoundOn");
					item.setIcon(R.drawable.volume);
					if(player.isPlaying())
						player.stop();
					if(gameView.player.isPlaying())
						gameView.player.pause();
				}else if(item.getTitle().equals("SoundOn")){
					item.setTitle("Mute");
					item.setIcon(R.drawable.mute);
					gameView.player.start();

				}
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			AlertDialog.Builder dialog= new AlertDialog.Builder(GameActivity.this).setTitle("退出游戏")
					.setMessage("确定退出游戏？")
					.setPositiveButton("是",new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent startMain = new Intent(Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							System.exit(0);
						}
					}).setNegativeButton("否", new DialogInterface.OnClickListener(){

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(GameActivity.this, "重新开始了游戏", Toast.LENGTH_LONG).show();
							gameView.startPlay();
						}

					});
			dialog.setIcon(R.drawable.icon);
			dialog.show();
		}
		return super.onKeyDown(keyCode, event);
	}

}